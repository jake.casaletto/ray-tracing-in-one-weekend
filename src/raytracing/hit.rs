use crate::primitives::ray::Ray;
use crate::raytracing::hit_result::HitResult;

pub trait Hit {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitResult>;
}

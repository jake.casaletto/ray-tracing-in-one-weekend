use crate::raytracing::hit::Hit;
use crate::rendering::scatter::Scatter;

pub mod raytrace_sphere;

pub trait RaytraceCollider: Hit + Scatter {}

use num_traits::Zero;

use crate::primitives::{point3::Point3, ray::Ray, Vec3};
use crate::rendering::material::Material;

#[derive(Debug, Copy, Clone)]
pub struct HitResult {
    point: Point3,
    normal: Vec3<f64>,
    t: f64,
    front_face: bool,
    material: Option<Material>,
}

impl HitResult {
    pub fn new(
        ray: &Ray,
        point: Point3,
        outward_normal: Vec3<f64>,
        t: f64,
        material: Option<Material>,
    ) -> Self {
        let front_face = Self::calculate_face(ray, outward_normal);
        let normal = front_face
            .then(|| outward_normal)
            .unwrap_or(-outward_normal);
        Self {
            point,
            normal,
            t,
            front_face,
            material,
        }
    }

    pub fn t(&self) -> f64 {
        self.t
    }

    pub fn normal(&self) -> Vec3<f64> {
        self.normal
    }

    pub fn point(&self) -> Point3 {
        self.point
    }

    pub fn front_face(&self) -> bool {
        self.front_face
    }

    pub fn update_material(&mut self, new_material: Option<Material>) {
        self.material = new_material;
    }

    pub fn material(&self) -> Option<Material> {
        self.material
    }

    fn calculate_face(ray: &Ray, outward_normal: Vec3<f64>) -> bool {
        ray.direction()
            .dot_product(&outward_normal)
            .lt(&f64::zero())
    }
}

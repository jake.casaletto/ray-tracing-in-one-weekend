use crate::rendering::scatter::Scatter;
use crate::{
    primitives::{color::Color, ray::Ray},
    scene::Scene,
};
use num_traits::{Float, Zero};

pub fn ray_color(ray: &Ray, scene: &Scene, depth: usize) -> Color {
    if depth.le(&usize::zero()) {
        return Color::new(0.0, 0.0, 0.0);
    }
    let hit = scene.cast_ray(ray, 0.001, f64::infinity());
    match hit {
        Some(hit_result) => {
            let material = hit_result.material();
            match material {
                Some(m) => {
                    let scatter_result = m.scatter(ray, hit_result);
                    match scatter_result {
                        Some(s) => s.attenuation() * ray_color(&s.ray(), scene, depth - 1),
                        None => Color::default(),
                    }
                }
                None => Color::new(1.0, 0.0, 1.0), // Magenta
            }
        }
        None => {
            let unit_direction = ray.direction().unit_vector().unwrap_or_default();
            let t = (unit_direction.y() + 1.0) * 0.5;
            (Color::new(1.0, 1.0, 1.0) * (1.0 - t)) + (Color::new(0.5, 0.7, 1.0) * t)
        }
    }
}

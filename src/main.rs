use rand::{thread_rng, Rng};
use std::num::NonZeroUsize;

use ray_tracing_basic::{
    primitives::{aspect_ratio::AspectRatio, color::Color, point3::Point3, Vec3},
    raytracing::raytrace_collider::{raytrace_sphere::RaytraceSphere, RaytraceCollider},
    rendering::{camera::Camera, image::formats::Ppm, material::Material, renderer::Renderer},
    scene::Scene,
    shape::sphere::Sphere,
};

const WIDTH: f64 = 1200.0;
const SAMPLES_PER_PIXEL: usize = 500;
const MAX_DEPTH: usize = 50;
const ASPECT_RATIO_X: f64 = 3.0;
const ASPECT_RATIO_Y: f64 = 2.0;
const V_FOV: f64 = 20.0;
const APERTURE: f64 = 0.1;
const DIST_TO_FOCUS: f64 = 10.0;

fn main() {
    let aspect_ratio = AspectRatio::new(ASPECT_RATIO_X, ASPECT_RATIO_Y);
    let objects = create_demo_objects();
    let scene = Scene::new(WIDTH, aspect_ratio, objects);

    let look_from = Point3::new(13.0, 2.0, 3.0);
    let look_at = Point3::default();
    let camera = Camera::new(
        V_FOV,
        aspect_ratio,
        look_from,
        look_at,
        Vec3::<f64>::new(0.0, 1.0, 0.0),
        APERTURE,
        DIST_TO_FOCUS,
    );
    let renderer = Renderer::new(scene, camera);
    let rendered_image = renderer.render(NonZeroUsize::new(SAMPLES_PER_PIXEL).unwrap(), MAX_DEPTH);
    let ppm = Ppm::new(rendered_image);
    print!("{}", ppm)
}

fn create_demo_objects() -> Vec<Box<dyn RaytraceCollider>> {
    let mut rng = thread_rng();

    let mat_ground = Material::new_lambertian(Color::new(0.5, 0.5, 0.5));
    let sphere_ground = Sphere::new(Point3::new(0.0, -1000.0, 0.0), 1000.0);
    let ground_collider = RaytraceSphere::new(sphere_ground, mat_ground);

    let mut objs: Vec<Box<dyn RaytraceCollider>> = vec![Box::new(ground_collider)];

    for a in -11..11 {
        for b in -11..11 {
            let choose_mat = rng.gen_range(0.0..1.0);
            let center = Point3::new(
                (a as f64) + (0.9 * rng.gen_range(0.0..1.0)),
                0.2,
                (b as f64) + (0.9 * rng.gen_range(0.0..1.0)),
            );

            if (center - Point3::new(4.0, 0.2, 0.0)).magnitude() > 0.9 {
                let material;
                if choose_mat < 0.8 {
                    // diffuse
                    let albedo = Color::new(
                        rng.gen_range(0.0..1.0),
                        rng.gen_range(0.0..1.0),
                        rng.gen_range(0.0..1.0),
                    );
                    material = Material::new_lambertian(albedo);
                    objs.push(Box::new(RaytraceSphere::new(
                        Sphere::new(center, 0.2),
                        material,
                    )));
                } else if choose_mat < 0.95 {
                    // metal
                    let albedo = Color::new(
                        rng.gen_range(0.5..=1.0),
                        rng.gen_range(0.5..=1.0),
                        rng.gen_range(0.5..=1.0),
                    );
                    let fuzz = rng.gen_range(0.0..=0.5);
                    material = Material::new_metal(albedo, fuzz);

                    objs.push(Box::new(RaytraceSphere::new(
                        Sphere::new(center, 0.2),
                        material,
                    )));
                } else {
                    // glass
                    material = Material::new_dielectric(1.5);
                    objs.push(Box::new(RaytraceSphere::new(
                        Sphere::new(center, 0.2),
                        material,
                    )));
                }
            }
        }
    }

    let mat_1 = Material::new_dielectric(1.5);
    objs.push(Box::new(RaytraceSphere::new(
        Sphere::new(Point3::new(0.0, 1.0, 0.0), 1.0),
        mat_1,
    )));

    let mat_2 = Material::new_lambertian(Color::new(0.4, 0.2, 0.1));
    objs.push(Box::new(RaytraceSphere::new(
        Sphere::new(Point3::new(-4.0, 1.0, 0.0), 1.0),
        mat_2,
    )));

    let mat_3 = Material::new_metal(Color::new(0.7, 0.6, 0.5), 0.0);
    objs.push(Box::new(RaytraceSphere::new(
        Sphere::new(Point3::new(4.0, 1.0, 0.0), 1.0),
        mat_3,
    )));

    objs
}

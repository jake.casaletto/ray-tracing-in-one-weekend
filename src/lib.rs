pub mod math;
pub mod primitives;
pub mod raytracing;
pub mod rendering;
pub mod scene;
pub mod shape;

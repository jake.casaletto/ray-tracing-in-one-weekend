pub use vec3::*;

// TODO: Everything in primitives can be its own well-formed crate
pub mod aspect_ratio;
pub mod color;
pub mod point3;
pub mod ray;
pub mod rgb_pixel;

use num_traits::{Float, Zero};
use std::ops::{Div, Mul};

#[derive(Debug, Copy, Clone)]
pub struct AspectRatio(f64, f64);

impl AspectRatio {
    pub fn new(x: f64, y: f64) -> Self {
        // TODO - actually deal with given 0 values with errors instead of this
        let x = x.le(&f64::zero()).then(|| f64::epsilon()).unwrap_or(x);
        let y = y.le(&f64::zero()).then(|| f64::epsilon()).unwrap_or(y);
        Self(x, y)
    }

    pub fn x(&self) -> f64 {
        self.0
    }

    pub fn y(&self) -> f64 {
        self.1
    }

    pub fn value(&self) -> f64 {
        self.x() / self.y()
    }

    pub fn calculate_width(&self, height: f64) -> f64 {
        self.value().mul(height)
    }

    pub fn calculate_height(&self, width: f64) -> f64 {
        width.div(self.value())
    }
}

// TODO tests

use std::num::NonZeroUsize;
use std::ops::Div;

use rand::{thread_rng, Rng};

use indicatif::{ProgressBar, ProgressStyle}; // TODO decouple

use crate::raytracing::ray_color::ray_color;
use crate::rendering::image::Image;
use crate::{
    primitives::color::Color, primitives::rgb_pixel::RgbPixel, rendering::camera::Camera,
    scene::Scene,
};

pub struct Renderer {
    scene: Scene,
    camera: Camera,
}

impl Renderer {
    pub fn new(scene: Scene, camera: Camera) -> Self {
        Self { scene, camera }
    }

    pub fn camera(&self) -> Camera {
        self.camera
    }

    // TODO casts, fallibility, general cleanup, wew
    pub fn render(&self, samples_per_pixel: NonZeroUsize, depth: usize) -> Image {
        let mut pixels = vec![];

        let bar_j = ProgressBar::new(self.scene.h() as u64);
        bar_j.set_style(
            ProgressStyle::default_bar()
                .template("[{elapsed_precise}] {bar:40.cyan/blue} {pos:>7}/{len:7} {msg}"),
        );
        bar_j.set_message("Image Rows");

        for j in (0..(self.scene.h() as usize)).rev() {
            bar_j.inc(1);
            for i in 0..(self.scene.w() as usize) {
                let mut output_color = Color::new(0.0, 0.0, 0.0);
                for _ in 0..samples_per_pixel.get() {
                    let (u, v) = self.calculate_u_v(i as f64, j as f64);
                    let ray = self.camera.get_ray(u, v);
                    output_color += ray_color(&ray, &self.scene, depth);
                }
                output_color = Self::scale_color_for_samples(output_color, samples_per_pixel);
                pixels.push(RgbPixel::from(output_color));
            }
        }

        bar_j.finish_using_style();
        Image::new(
            self.scene.w() as usize,
            self.camera.aspect_ratio(),
            pixels,
            samples_per_pixel,
            depth,
        )
    }

    fn calculate_u_v(&self, i: f64, j: f64) -> (f64, f64) {
        let mut rng = thread_rng();
        let u = (i + rng.gen_range(0.0..1.0)) / (self.scene.w() as f64 - 1.0);
        let v = (j + rng.gen_range(0.0..1.0)) / (self.scene.h() as f64 - 1.0);
        (u, v)
    }

    // TODO add this to color
    fn scale_color_for_samples(color: Color, samples_per_pixel: NonZeroUsize) -> Color {
        let mut r = color.x();
        let mut g = color.y();
        let mut b = color.z();

        let scale = 1.0.div(samples_per_pixel.get() as f64);
        r *= scale;
        g *= scale;
        b *= scale;
        let (r, g, b) = Self::gamma_2_correct(r, g, b);
        Color::new(r, g, b)
    }

    // TODO add this to color
    fn gamma_2_correct(r: f64, g: f64, b: f64) -> (f64, f64, f64) {
        let r = r.sqrt();
        let g = g.sqrt();
        let b = b.sqrt();
        (r, g, b)
    }
}

use crate::primitives::aspect_ratio::AspectRatio;
use crate::primitives::rgb_pixel::RgbPixel;
use std::num::NonZeroUsize;
use std::ops::Div;

pub mod formats;

#[derive(Debug, Clone)]
pub struct Image {
    height: usize,
    width: usize,
    pixels: Vec<RgbPixel>,
    aspect_ratio: AspectRatio,
    samples_per_pixel: NonZeroUsize,
    depth: usize,
}

impl Image {
    pub fn new(
        width: usize,
        aspect_ratio: AspectRatio,
        pixels: Vec<RgbPixel>,
        samples_per_pixel: NonZeroUsize,
        depth: usize,
    ) -> Self {
        Self {
            height: aspect_ratio.calculate_height(width as f64) as usize, // TODO casts
            width,
            aspect_ratio,
            pixels,
            samples_per_pixel,
            depth,
        }
    }

    pub fn new_rainbow(width: usize, aspect_ratio: AspectRatio) -> Self {
        Self::rainbow(
            aspect_ratio.calculate_height(width as f64) as usize,
            width,
            aspect_ratio,
        )
    }

    fn rainbow(h: usize, w: usize, aspect_ratio: AspectRatio) -> Self {
        let mut pixels = vec![];
        for j in (0..h).rev() {
            for i in 0..w {
                let color = RgbPixel::try_new(
                    (i as f64).div((w as f64) - 1.0),
                    (j as f64).div((h as f64) - 1.0),
                    0.25,
                )
                .unwrap_or_default();
                pixels.push(color);
            }
        }
        Self {
            height: h,
            width: w,
            pixels,
            aspect_ratio,
            samples_per_pixel: NonZeroUsize::new(1).unwrap(),
            depth: 1,
        }
    }

    pub fn h(&self) -> usize {
        self.height
    }

    pub fn w(&self) -> usize {
        self.width
    }

    pub fn aspect_ratio(&self) -> AspectRatio {
        self.aspect_ratio
    }

    // TODO: Break out into organized iterations
    pub fn pixels(&self) -> &Vec<RgbPixel> {
        &self.pixels
    }

    // TODO: fallible set pixel()
    // TODO: this can create a pixel vector of different size than image hxw; should be made fallible
    pub fn set_pixels(&mut self, pixels: Vec<RgbPixel>) {
        self.pixels = pixels;
    }
}

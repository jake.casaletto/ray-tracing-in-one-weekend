use num_traits::Zero;

use crate::math::random_in_unit_sphere;
use crate::{
    primitives::{color::Color, ray::Ray, Vec3},
    raytracing::hit_result::HitResult,
    rendering::scatter::{Scatter, ScatterResult},
};

#[derive(Debug, Copy, Clone)]
pub struct MetalMaterial {
    albedo: Color,
    fuzz: f64,
}

impl MetalMaterial {
    pub fn new(albedo: Color, fuzz: f64) -> Self {
        Self {
            albedo,
            fuzz: (fuzz.min(1.0)).max(-1.0),
        }
    }

    pub fn albedo(&self) -> Color {
        self.albedo
    }

    pub fn fuzz(&self) -> f64 {
        self.fuzz
    }

    pub fn set_albedo(&mut self, new_albedo: Color) {
        self.albedo = new_albedo
    }
}

impl Scatter for MetalMaterial {
    fn scatter(&self, r_in: &Ray, hit_result: HitResult) -> Option<ScatterResult> {
        let reflected_vector: Vec3<f64> = r_in
            .direction()
            .unit_vector()
            .unwrap_or_default()
            .reflect(&hit_result.normal());
        let scattered_ray = Ray::new(
            hit_result.point(),
            reflected_vector + (random_in_unit_sphere() * self.fuzz()),
        );
        if scattered_ray
            .direction()
            .dot_product(&hit_result.normal())
            .gt(&f64::zero())
        {
            Some(ScatterResult::new(scattered_ray, self.albedo()))
        } else {
            None
        }
    }
}

use crate::math::{is_vec3_near_zero, random_in_unit_sphere};
use crate::primitives::color::Color;
use crate::primitives::ray::Ray;
use crate::primitives::Vec3;
use crate::raytracing::hit_result::HitResult;
use crate::rendering::scatter::{Scatter, ScatterResult};

#[derive(Debug, Copy, Clone)]
pub struct LambertianMaterial(Color);

impl LambertianMaterial {
    pub fn new(albedo: Color) -> Self {
        Self(albedo)
    }

    pub fn albedo(&self) -> Color {
        self.0
    }

    pub fn set_albedo(&mut self, new_albedo: Color) {
        self.0 = new_albedo
    }
}

impl Scatter for LambertianMaterial {
    fn scatter(&self, _: &Ray, hit_result: HitResult) -> Option<ScatterResult> {
        let mut scatter_direction: Vec3<f64> =
            hit_result.normal() + random_in_unit_sphere().unit_vector().unwrap_or_default();

        if is_vec3_near_zero(scatter_direction) {
            scatter_direction = hit_result.normal();
        };

        let scatter_ray = Ray::new(hit_result.point(), scatter_direction);
        Some(ScatterResult::new(scatter_ray, self.albedo()))
    }
}

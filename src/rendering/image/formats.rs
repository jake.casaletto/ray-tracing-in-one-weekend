use std::fmt::{Display, Formatter, Result as FormatterResult};

use crate::rendering::image::Image;

#[derive(Debug, Clone)]
pub struct Ppm(Image);

impl Ppm {
    pub fn new(image: Image) -> Self {
        Self(image)
    }

    pub fn header(&self) -> String {
        format!("P3\n{} {}\n255\n", self.0.width, self.0.height)
    }
}

impl Display for Ppm {
    fn fmt(&self, f: &mut Formatter<'_>) -> FormatterResult {
        write!(f, "{}", self.header())?;
        for pixel in &self.0.pixels {
            let byte_values = pixel.byte_values();
            write!(f, "{} {} {}\n", byte_values.0, byte_values.1, byte_values.2)?;
        }
        Ok(())
    }
}
